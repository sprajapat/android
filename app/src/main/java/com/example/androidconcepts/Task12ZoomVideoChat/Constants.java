package com.example.androidconcepts.Task12ZoomVideoChat;

public interface Constants {
    public final static String APP_KEY = "mbHCq49XBTdDRG5ogIAfNVJNx8xp2RiRzv1H";

    // Change it to your APP Secret
    public final static String APP_SECRET = "qYhOPSpSAOPEUtH1BGzIYWncDMyDQenK26a2";

// ==============
    /* If you would like to ask your user to login with their own credentials, you don't need to fill up the following. */

    // Change it to your user ID
    public final static String USER_ID = "Your user ID from REST API";

    // Change it to your zoom token
    public final static String ZOOM_TOKEN = "Your zoom token from REST API";

    // Change it to your zoom access token(ZAK)
    public final static String ZOOM_ACCESS_TOKEN = "Your zoom access token(ZAK) from REST API";

}
