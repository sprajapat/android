package com.example.androidconcepts.Task17BarPieChartandPIP_mode;

import android.app.ActivityOptions;
import android.app.PictureInPictureParams;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Rational;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.R;

public class ChartActivity extends ParentActivity {
    Button btn1, btn2, btn3;
    LinearLayout linearLayout;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_buttons);
        findViewById();
        init();
    }

    private void findViewById() {
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        linearLayout = findViewById(R.id.linearLayout);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void init() {
        btn1.setText("Bar Chart");
        btn2.setText("Pie Chart");
        btn3.setText("Picture Mode");
        initMainBack();

        imgMainBack.setOnClickListener(v -> {
            Resources res = getApplicationContext().getResources();
            TransitionDrawable transition = (TransitionDrawable) ResourcesCompat.getDrawable(res, R.drawable.expand_collapse2, null);
            imgMainBack.setImageDrawable(transition);
            assert transition != null;
            transition.startTransition(10000);
        });

        btn3.setOnClickListener(v -> {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                Rational rational = new Rational(linearLayout.getWidth(), linearLayout.getHeight());
                PictureInPictureParams params = new PictureInPictureParams.Builder().setAspectRatio(rational).build();
                btn3.setVisibility(View.INVISIBLE);
                enterPictureInPictureMode(params);
            }
        });

        btn1.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), Chart2Activity.class);
            intent.setAction("BARCHART");
            startActivity(intent);
        });
        btn2.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), Chart2Activity.class);
            intent.setAction("PIECHART");
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });
    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode, Configuration newConfig) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode);
        if (isInPictureInPictureMode) {
        } else {
            btn3.setVisibility(View.VISIBLE);
        }
    }
}
