package com.example.androidconcepts.Task7DrawerMenu;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.R;

/**
 * Created on 22feb 2020
 * author suraj
 * <p>
 * Task1 package
 * 1.create a DrawerMenu
 * <p>
 * Content of Task6 Package
 * 1.DrawerActivity class
 * 2.AlarmFragment Class
 */
public class DrawerActivity extends ParentActivity {
    DrawerLayout drawerLayout;
    FrameLayout master_fragment_container;
    private Fragment currentFragment;
    TextView txtAlarm1, txtAlarm2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        findViewById();
        init();
    }

    private void findViewById() {
        drawerLayout = findViewById(R.id.drawerLayout);
        master_fragment_container = findViewById(R.id.master_fragment_container);
        txtAlarm1 = findViewById(R.id.txtAlarm1);
        txtAlarm2 = findViewById(R.id.txtAlarm2);

    }

    private void init() {
        setActivityTitle(getString(R.string.alarm_activity));
        initMainBack();

        imgMainBack.setImageResource(R.drawable.ic_menu);
        imgMoreMenu.setVisibility(View.GONE);
        imgMainBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        txtAlarm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (!(currentFragment instanceof AlarmFragment)) {
                    changeFragmentdashboard(new AlarmFragment());
                }
            }
        });

        txtAlarm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (!(currentFragment instanceof Alarm2Fragment)) {
                    changeFragmentdashboard(new Alarm2Fragment());
                }
            }
        });

        AlarmFragment alarmFragment = new AlarmFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.master_fragment_container, alarmFragment)
                .commit();
    }

    public void changeFragmentdashboard(Fragment targetFragment) {
        currentFragment = targetFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.master_fragment_container, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


}
