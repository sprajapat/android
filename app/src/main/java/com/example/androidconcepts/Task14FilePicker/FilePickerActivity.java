package com.example.androidconcepts.Task14FilePicker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

import com.example.androidconcepts.R;

public class FilePickerActivity extends AppCompatActivity {
    Button btn1, btn2, btn3, btn4, btn5;

    private final static int IMAGE_PICK = 1;
    private final static int VIDEO_PICK = 2;
    private final static int AUDIO_PICK = 3;
    private final static int FILE_PICK = 4;
    private final static int ANY_PICK = 5;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_buttons);
        findViewById();
        init();
    }

    private void findViewById() {
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);

    }

    private void init() {
        btn1.setText("Image Pick");
        btn2.setText("Video Pick");
        btn3.setText("Audio Pick");
        btn4.setText("File Pick");
        btn5.setText("Any Pick");

        btn1.setOnClickListener((View v) -> {
            Intent chooseFile = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            chooseFile.setType("image/*");

            chooseFile = Intent.createChooser(chooseFile, "Choose a image");
            startActivityForResult(chooseFile, IMAGE_PICK);
        });
        btn2.setOnClickListener((View v) -> {
            Intent chooseFile = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            chooseFile.setType("video/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a video");
            startActivityForResult(chooseFile, VIDEO_PICK);
        });

        btn3.setOnClickListener((View v) -> {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
            chooseFile.setType("audio/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a audio");
            startActivityForResult(chooseFile, AUDIO_PICK);
        });
        btn4.setOnClickListener((View v) -> {
            Intent chooseFile = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
            chooseFile.setType("file/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, FILE_PICK);
        });
        btn5.setOnClickListener((View v) -> {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose any");
            startActivityForResult(chooseFile, ANY_PICK);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == IMAGE_PICK && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            imgView.setImageBitmap(imageBitmap);
//        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
//            videoUri = data.getData();
//            playbackRecordedVideo();
//        }

    }
}
