package com.example.androidconcepts.Task2LifeCycle.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.androidconcepts.R;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
