package com.example.androidconcepts.Task2LifeCycle.Utils;
/*
 *
 * created on 14feb 2020
 *Util class
 *NotificationScreen Menthods Custom and Default
 *
 * */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task1Notification.Activity.MainActivity;
import com.example.androidconcepts.Task1Notification.Services.MyMusicService;
import com.example.androidconcepts.Task1Notification.Utils.CommonConstants;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationScreen {
    public static RemoteViews remoteViews;
    public static NotificationCompat.Builder builder;
    public static NotificationManager notificationmanager;
    public static android.app.Notification notification;

    public static void DefaultNotification(Context context, String packageName, int layout, String title, String text, String ticker) {
        String strtitle = title;
        String strtext = text;

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create NotificationScreen using NotificationCompat.Builder

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setTicker(ticker)
                .setContentTitle(title)
                .setContentText(text)
                .addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
                .setContentIntent(pIntent)
                .setAutoCancel(true);

        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build NotificationScreen with NotificationScreen Manager
        notificationmanager.notify(0, builder.build());
    }


    public static Notification CustomNotification(Context context, String packageName, int layout, String title, String text, String ticker) {

        remoteViews = new RemoteViews(packageName, layout);

        Intent serviceIntent = new Intent(context, MainActivity.class);
        serviceIntent.setAction(CommonConstants.ACTION.MAIN_ACTION);
        serviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                serviceIntent, 0);

        Intent playIntent = new Intent(context, MyMusicService.class);
        playIntent.setAction(CommonConstants.ACTION.PLAY_ACTION);

        PendingIntent pplayIntent = PendingIntent.getService(context, 0,
                playIntent, 0);

        Intent closeIntent = new Intent(context, MyMusicService.class);
        closeIntent.setAction(CommonConstants.ACTION.STOPFOREGROUND_ACTION);

        PendingIntent pcloseIntent = PendingIntent.getService(context, 0,
                closeIntent, 0);

//        Intent deleteIntent = new Intent(context, MyMusicService.class);
//        deleteIntent.setAction(CommonConstants.ACTION.STOPFOREGROUND_ACTION);
//        PendingIntent deletePendingIntent = PendingIntent.getService(context, 0, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        remoteViews.setImageViewResource(R.id.imgRewind, R.drawable.ic_rewind);
        remoteViews.setImageViewResource(R.id.imgPlay, R.drawable.ic_play);
        remoteViews.setImageViewResource(R.id.imgForward, R.drawable.ic_forward);
        remoteViews.setImageViewResource(R.id.imgPause, R.drawable.ic_pause);
        remoteViews.setOnClickPendingIntent(R.id.imgPlay, pplayIntent);
        remoteViews.setOnClickPendingIntent(R.id.imgPause, pcloseIntent);


        notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setTicker(ticker)
                .setAutoCancel(true)
                .setPriority(android.app.Notification.PRIORITY_MIN)
                .setContentIntent(pendingIntent)
                .setContent(remoteViews).build();

        createNotificationChannel(context);
//        notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.notify(0, notification);

        return notification;
    }
    public static void changeNotification() {
        if (MyMusicService.playPause) {
            remoteViews.setViewVisibility(R.id.imgPlay, View.GONE);
            remoteViews.setViewVisibility(R.id.imgPause, View.VISIBLE);
        } else {
            remoteViews.setViewVisibility(R.id.imgPlay, View.VISIBLE);
            remoteViews.setViewVisibility(R.id.imgPause, View.GONE);
        }
        notificationmanager.notify(0, notification);
    }

    public static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CommonConstants.CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notificationmanager = context.getSystemService(NotificationManager.class);
            notificationmanager.createNotificationChannel(serviceChannel);
        } else {
            notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        }
    }
}
