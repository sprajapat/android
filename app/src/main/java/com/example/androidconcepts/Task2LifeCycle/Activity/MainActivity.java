package com.example.androidconcepts.Task2LifeCycle.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task1Notification.Services.MyMusicService;

/**
 * Created on 18feb 2020
 * author suraj
 * <p>
 * Task2 package
 * 1.
 * <p>
 * Content of Task1 Package
 * 1.AlarmActivity class
 * 2.activity lifecycle observe
 * 3.
 * 4.
 */

public class MainActivity extends AppCompatActivity {
    Button button, button2;
    public ImageView imgPause, imgPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }


    public void findViewById() {
        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        imgPlay = findViewById(R.id.imgPlay);
        imgPause = findViewById(R.id.imgPause);
    }

    private void init() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviceIntent = new Intent(getBaseContext(), Main2Activity.class);
//                serviceIntent.setAction(CommonConstants.ACTION.STARTFOREGROUND_ACTION);
//                startService(serviceIntent);
                startActivity(serviceIntent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(getBaseContext(), MyMusicService.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Start", "on Start method is called");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("Post resume", "on Post resume method is called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "onResume method is called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onRestart", "onRestart method is called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop", "onStop method is called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause", "onPause method is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy", "onDestroy method is called");
    }
}
