package com.example.androidconcepts;

import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

public class MySettingsActivity extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_layout);
        initMainBack();

        setActivityTitle("Settings");

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.llContainer, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            bindPreferenceSummaryToValue(findPreference("general"));
            bindPreferenceSummaryToValue(findPreference("notification"));
            bindPreferenceSummaryToValue(findPreference("language"));
        }
    }

    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringValue = newValue.toString();
            Log.e("stringValue", stringValue);

            if (preference instanceof Preference) {
                if (preference.getKey().equals("general")) {
                    // update the changed gallery name to summary filed
                    Log.e("general", "selected");
                    preference.setSummary("aaaaaaa");
                } else if (preference.getKey().equals("notification")) {
                    preference.setSummary("aaaaaadss");
                } else if (preference.getKey().equals("language")) {
                    preference.setSummary("dddf");
                }
            }
            return true;
        }
    };

    @Override
    public ParentActivity getActivity() {
        return super.getActivity();
    }
}
