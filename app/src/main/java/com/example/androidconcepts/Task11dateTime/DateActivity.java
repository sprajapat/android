package com.example.androidconcepts.Task11dateTime;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.androidconcepts.R;

import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateActivity extends AppCompatActivity {
    TextView txtDate, txtDate2, txtTime, txtChangedDate;
    Handler hand = new Handler();
    Calendar calendar;
    EditText edtDate, edtFormat, edtChangeFormat;
    Button btnChangedate;
    private long startTime = 0L;

    private Handler customHandler = new Handler();

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        findViewById();
        init();
    }


    private void findViewById() {
        txtDate = findViewById(R.id.txtDate);
        txtDate2 = findViewById(R.id.txtDate2);
        txtTime = findViewById(R.id.txtTime);
        edtDate = findViewById(R.id.edtDate);
        edtFormat = findViewById(R.id.edtFormat);
        edtChangeFormat = findViewById(R.id.edtChangeFormat);
        txtChangedDate = findViewById(R.id.txtChangedDate);
        btnChangedate = findViewById(R.id.btnChangedate);
    }

    private void init() {
        setupDate1();
        setupDate2();
        setupTime1();

        btnChangedate.setOnClickListener(v -> {
            String convertedDate = changeDateFormate(edtDate.getText().toString(), edtFormat.getText().toString(), edtChangeFormat.getText().toString());
            txtChangedDate.setText(convertedDate);
        });

        txtDate2.setOnClickListener((View v) -> {
            showDialog(01);
        });
        txtTime.setOnClickListener((View v) -> {
            showDialog(02);
        });

        startTime = SystemClock.uptimeMillis();
    }

    private String changeDateFormate(String date, String fromFormate, String toFormat) {
        SimpleDateFormat fromDateFormat = new SimpleDateFormat(fromFormate, Locale.getDefault());
        try {
            Date fromDate = fromDateFormat.parse(date);
            SimpleDateFormat toDateFormat = new SimpleDateFormat(toFormat, Locale.getDefault());
            assert fromDate != null;
            return toDateFormat.format(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void setupDate1() {
        final Date myDate = new Date();
        final long expirationDate = myDate.getTime() + TimeUnit.DAYS.toMillis(0);
        myDate.setTime(expirationDate);
        String myFormattedDate = DateFormat.getDateInstance().format(myDate);
        txtDate.setText(myFormattedDate);
    }

    private void setupDate2() {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        Integer[] ab = {day, month, year};

        String dateString = String.format("%02d-%02d-%d", ab);
        txtDate2.setText(dateString);
    }

    private void setupTime1() {
        txtTime.setText(new StringBuilder().append(calendar.get(Calendar.HOUR_OF_DAY) + ":").append(String.format("%02d", calendar.get(Calendar.MINUTE))));
    }

    Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            int hours = mins / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);

            txtTime.setText("" + hours + ":"
                    + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs));
            customHandler.postDelayed(this, 0);
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 01) {
            return new DatePickerDialog(this, myDateListener, 2020, 01, 12);
        } else if (id == 02) {
            return new TimePickerDialog(this, timeSetListener, 00, 00, false);
        }
        return null;
    }


    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            showTime(hourOfDay, minute);
        }
    };

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            showDate(year, month + 1, dayOfMonth);

        }
    };

    private void showDate(int year, int month, int day) {
        txtDate2.setText(new StringBuilder().append(day).append("/")
                .append(String.format("%02d", month)).append("/").append(year));
    }

    private void showTime(int hourOfDay, int minute) {
        txtTime.setText(new StringBuilder().append(String.format("%02d", hourOfDay)).append(":")
                .append(String.format("%02d", minute)));
    }
}
