package com.example.androidconcepts.Task18Animation;

import android.app.ActivityOptions;
import android.app.PictureInPictureParams;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Rational;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task17BarPieChartandPIP_mode.Chart2Activity;

public class AnimationActivity extends AppCompatActivity {
    Button btn1, btn2, btn3, btn4, btn5;
    LinearLayout linearLayout;
    AnimationDrawable rocketAnimation;
    public ImageView imgMainBack;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_buttons);
        findViewById();
        init();
    }

    private void findViewById() {
        imgMainBack = findViewById(R.id.imgBack);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        linearLayout = findViewById(R.id.linearLayout);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void init() {
        btn1.setText("Animation 1");
        btn2.setText("Animation 2 Next Activity");
        btn3.setText("Picture Mode");
        btn4.setText("Anim Mode2");
        btn5.setText("Fade In/Out");
//        initMainBack();

        btn1.setOnClickListener(view -> {
            Resources res = getResources();
            TransitionDrawable transition = (TransitionDrawable) ResourcesCompat.getDrawable(res, R.drawable.expand_collapse1, null);
            imgMainBack.setImageDrawable(transition);
            transition.startTransition(1000);
            transition.setCrossFadeEnabled(false); // call public methods

        });
        btn2.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), Chart2Activity.class);
            intent.setAction("PIECHART");
            startActivity(intent/*, ActivityOptions.makeSceneTransitionAnimation(this).toBundle()*/);
        });

        btn3.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Rational rational = new Rational(linearLayout.getWidth(), linearLayout.getHeight());
                PictureInPictureParams params = new PictureInPictureParams.Builder().setAspectRatio(rational).build();
                btn3.setVisibility(View.INVISIBLE);
                enterPictureInPictureMode(params);
            }
        });

        btn4.setOnClickListener(v -> {
            imgMainBack.setBackgroundResource(R.drawable.expand_collapse2);
            rocketAnimation = (AnimationDrawable) imgMainBack.getBackground();
            rocketAnimation.start();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    rocketAnimation.stop();
                }
            }, 3000);
        });

        btn5.setOnClickListener(v -> {

            Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
            imgMainBack.startAnimation(fadeOut);

            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    imgMainBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu));
                    Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    imgMainBack.startAnimation(fadeIn);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        });


    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode, Configuration newConfig) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode);
        if (isInPictureInPictureMode) {
        } else {
            btn3.setVisibility(View.VISIBLE);
        }
    }
}
