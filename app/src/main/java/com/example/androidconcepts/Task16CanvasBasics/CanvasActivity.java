package com.example.androidconcepts.Task16CanvasBasics;

import androidx.appcompat.app.AppCompatActivity;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.androidconcepts.R;

import java.io.IOException;
import java.util.Scanner;

public class CanvasActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    MyView myView;
    GLSurfaceView mySurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas);

        linearLayout = findViewById(R.id.linearLayout);
        mySurfaceView = findViewById(R.id.my_surface_view);

        myView = new MyView(this);
        linearLayout.addView(myView);

        mySurfaceView.setEGLContextClientVersion(2);
        try {
            Scanner scanner = new Scanner(getApplicationContext().getAssets().open(""));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}