package com.example.androidconcepts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ParentActivity extends AppCompatActivity {
    public ImageView imgMoreMenu;
    public ImageView imgMainBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ParentActivity getActivity() {
        return ParentActivity.this;
    }

    public void setActivityTitle(String titleText) {
        try {
            TextView txtActivityTitle = findViewById(R.id.txtTitle);
            txtActivityTitle.setText(Utils.nullSafe(titleText));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMainBack() {
        try {
            imgMoreMenu = findViewById(R.id.imgMenu);
            imgMainBack = findViewById(R.id.imgBack);

            imgMainBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
