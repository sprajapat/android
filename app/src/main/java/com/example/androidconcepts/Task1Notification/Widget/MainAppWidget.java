package com.example.androidconcepts.Task1Notification.Widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;

import com.example.androidconcepts.R;

/**
 * Created on 21feb 2020
 * author suraj
 *
 * Task1 package
 * 1.Implement WidgetService
 *
 * */
public class MainAppWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.main_app_widget);
        views.setImageViewResource(R.id.imgRewind, R.drawable.ic_rewind);
        views.setImageViewResource(R.id.imgPlay, R.drawable.ic_play);
        views.setImageViewResource(R.id.imgForward, R.drawable.ic_forward);
        views.setImageViewResource(R.id.imgPause, R.drawable.ic_pause);
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

