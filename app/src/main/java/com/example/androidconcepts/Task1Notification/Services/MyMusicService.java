package com.example.androidconcepts.Task1Notification.Services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task1Notification.Utils.CommonConstants;
import com.example.androidconcepts.Task1Notification.Notification.NotificationScreen;

/*
 * Created on 14feb 2020
 * author suraj
 *
 * Task--> play music using notification
 * */

public class MyMusicService extends Service {
    MediaPlayer player;
    public static boolean playPause;

    @Override
    public IBinder onBind(Intent intent) {
        Log.e("onBind", "onBind method is called");
        return null;

    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("onCreate", "onCreate method is called");
        //media player created
        player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
        player.setLooping(true); // Set looping
        player.setVolume(100, 100);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e("onStartCommand", "onStartCommand method is called");
        //media player start
        if (intent.getAction().equals(CommonConstants.ACTION.STARTFOREGROUND_ACTION)) {

            NotificationScreen.CustomNotification(getApplicationContext(), getPackageName(), R.layout.custom_notification, "", "", "");
            Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

        } else if (intent.getAction().equals(CommonConstants.ACTION.PLAY_ACTION)) {
            player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
            player.setLooping(true); // Set looping
            player.setVolume(100, 100);
            player.start();
            playPause = true;
            NotificationScreen.changeNotification();
            Toast.makeText(this, "Music Play", Toast.LENGTH_SHORT).show();
            Log.e("clicked", "Music Play");

        } else if (intent.getAction().equals(CommonConstants.ACTION.PAUSE_ACTION)) {
            playPause = false;
            Log.e("clicked", "Music Pause");
            Toast.makeText(this, "Music Pause", Toast.LENGTH_SHORT).show();
            NotificationScreen.changeNotification();
            player.stop();

        } else if (intent.getAction().equals(CommonConstants.ACTION.STOPFOREGROUND_ACTION)) {
            Log.i("clicked", "Received Stop Foreground Intent");
            Toast.makeText(this, "Music Service Stopped", Toast.LENGTH_SHORT).show();
            stopForeground(true);
            stopSelf();
//            onDestroy();
            return START_NOT_STICKY;
        }
        Log.i("clicked", "start Sticky");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.e("onDestroy", "onDestroy method is called");
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        stopSelf();
        stopForeground(true);
//        media player stop
        player.stop();
        player.release();
        super.onDestroy();
    }


}
