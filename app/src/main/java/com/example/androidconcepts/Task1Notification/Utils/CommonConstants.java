package com.example.androidconcepts.Task1Notification.Utils;

public class CommonConstants {
    public static boolean isPlay;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    public interface ACTION {
        public static String MAIN_ACTION = "com.example.androidconcepts.action.main";
        public static String INIT_ACTION = "com.example.androidconcepts.action.init";
        public static String PREV_ACTION = "com.example.androidconcepts.action.prev";
        public static String PLAY_ACTION = "com.example.androidconcepts.action.play";
        public static String PAUSE_ACTION = "com.example.androidconcepts.action.pause";
        public static String NEXT_ACTION = "com.example.androidconcepts.action.next";
        public static String STARTFOREGROUND_ACTION = "com.example.androidconcepts.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.example.androidconcepts.action.stopforeground";
    }
}
