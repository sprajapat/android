package com.example.androidconcepts.Task1Notification.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task1Notification.Services.MyMusicService;
import com.example.androidconcepts.Task1Notification.Utils.CommonConstants;

/**
 * Created on 14feb 2020
 * author suraj
 *
 * Task1 package
 * 1.create a service which play music and show notification
 * 2.play music using notification play and pause buttons
 *
 *
 * Content of Task1 Package
 * 1.MyMusicService class
 * 2.AlarmActivity class
 * 3.NotificationScreen Class
 * 4.CommonConstants class
 *
 * */

public class MainActivity extends AppCompatActivity {
    Button button, button2;
    public ImageView imgPause, imgPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        Log.e("OnCreate", "OnCreate method is called");

    }


    public void findViewById() {
        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        imgPlay = findViewById(R.id.imgPlay);
        imgPause = findViewById(R.id.imgPause);
    }

    private void init() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent serviceIntent = new Intent(getBaseContext(), MyMusicService.class);
                serviceIntent.setAction(CommonConstants.ACTION.STARTFOREGROUND_ACTION);
                startService(serviceIntent);

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(getBaseContext(), MyMusicService.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Start", "on Start method is called");
        init();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("PostResume", "on PostResume method is called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "onResume method is called");
    }
}
