package com.example.androidconcepts.Task9MultiThreading.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.R;

/**
 * Created on 24feb 2020
 * author suraj
 * <p>
 * Task9 package
 * 1.MultiThreading
 * <p>
 * Content of Task6 Package
 * 1.MainActivity class
 * 2.
 */
public class MainActivity extends ParentActivity {
    EditText editQuery;
    TextView txtView, txtView1;
    boolean twice = false;
    Thread t = null;
    Button btnClick;
    Handler hand = new Handler();

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        findViewById();
        init();
    }

    private void findViewById() {
        editQuery = findViewById(R.id.editQuery);
        txtView = findViewById(R.id.txtView);
        txtView1 = findViewById(R.id.txtView1);
        btnClick = findViewById(R.id.btnClick);
    }

    private void init() {
        setActivityTitle(getString(R.string.thread_activity));
        initMainBack();
        hand.postDelayed(run, 1000);

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runthread();
                runthread1();
            }
        });
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            updateTime();
        }
    };

    public void updateTime() {
        txtView.setText("" + (Integer.parseInt(txtView.getText().toString()) - 1));
        if (Integer.parseInt(txtView.getText().toString()) == 0) {
            btnClick.setVisibility(View.VISIBLE);
        } else {
            hand.postDelayed(run, 1000);
        }
    }

    private void runthread1() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                txtView1.setText("Thread");
            }
        });
    }

    private void runthread() {
        twice = true;
        if (twice) {
            final String s1 = editQuery.getText().toString();
            t = new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtView.setText(t.getName());
                            twice = false;
                        }
                    });
                }
            });
            t.start();
//            t.setName(s1);
            t.setPriority(Thread.MAX_PRIORITY);
        }
    }


}