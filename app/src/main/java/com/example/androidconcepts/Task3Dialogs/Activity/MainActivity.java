package com.example.androidconcepts.Task3Dialogs.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task1Notification.Services.MyMusicService;
import com.example.androidconcepts.Task1Notification.Utils.CommonConstants;

/**
 * Created on 19feb 2020
 * author suraj
 * <p>
 * Task1 package
 * 1.create a Dialogs
 * <p>
 * <p>
 * Content of Task1 Package
 * 2.PopupWindow Class
 */

public class MainActivity extends AppCompatActivity {
    Button btnPopUpWindow, btnDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dialogs);
        findViewById();

    }


    public void findViewById() {
        btnPopUpWindow = findViewById(R.id.btnPopUpWindow);
        btnDialog = findViewById(R.id.btnDialog);
    }

    private void init() {
        btnPopUpWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

}
