package com.example.androidconcepts.Task6Actions.Receiver;
/**
 * Created on 21feb 2020
 * author suraj
 * BroadCastReceiver Class
 *
 *
 * must declare receiver in menifest
 *
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

import com.example.androidconcepts.R;

public class MyBroadcastReceiver extends BroadcastReceiver {
    MediaPlayer mp;

    @Override
    public void onReceive(Context context, Intent intent) {
        mp = MediaPlayer.create(context, R.raw.music_one);
        mp.start();
        Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show();
    }
}
