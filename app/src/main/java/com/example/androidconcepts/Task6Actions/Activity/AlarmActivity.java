package com.example.androidconcepts.Task6Actions.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.MySettingsActivity;
import com.example.androidconcepts.R;
import com.example.androidconcepts.Task6Actions.Receiver.MyBroadcastReceiver;

/**
 * Created on 21feb 2020
 * author suraj
 * <p>
 * Task1 package
 * 1.create a Alarm Service using BroadCast
 * 2.create a Alarm Service using Intent
 * <p>
 * Content of Task6 Package
 * 1.AlarmActivity class
 * 2.MyBroadCastReceiver Class
 */
public class AlarmActivity extends ParentActivity {
    Button btnAlarmBrocast, btnAlarmIntent;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dialogs);
        findViewById();
        init();
    }

    private void findViewById() {
        btnAlarmBrocast = findViewById(R.id.btnPopUpWindow);
        btnAlarmIntent = findViewById(R.id.btnDialog);
    }

    private void init() {
        btnAlarmBrocast.setText(getString(R.string.broadcast_alarm));
        btnAlarmIntent.setText(getString(R.string.intent_alarm));

        setActivityTitle(getString(R.string.alarm_activity));
        initMainBack();

        btnAlarmBrocast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BroadCastAlarm();
            }
        });

        imgMoreMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent languageIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
//                startActivity(languageIntent);
                Intent intent = new Intent(getApplicationContext(), MySettingsActivity.class);
                startActivity(intent);
            }
        });

        btnAlarmIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CaptureImgActivity.class);
                startActivity(intent);
                IntentAlarm("SetAlarm", 1, 25);
            }
        });

    }

    public void BroadCastAlarm() {
        //Broadcast Receiver
        Intent intent = new Intent(this, MyBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (10 * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + 10 + " seconds", Toast.LENGTH_LONG).show();
    }

    private void IntentAlarm(String setAlarm, int i, int i1) {
        //not Working
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "New Alarm");
        intent.putExtra(AlarmClock.EXTRA_HOUR, 0);
        intent.putExtra(AlarmClock.EXTRA_MINUTES, 5);
        intent.putExtra(AlarmClock.EXTRA_RINGTONE, R.raw.music_one);
        startActivity(intent);
    }


}

