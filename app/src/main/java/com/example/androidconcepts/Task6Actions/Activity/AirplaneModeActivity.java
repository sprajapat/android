package com.example.androidconcepts.Task6Actions.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.MySettingsActivity;
import com.example.androidconcepts.R;

/**
 * Created on 24feb 2020
 * author suraj
 * <p>
 * Task6 package
 *
 *
 * <p>
 * Content of Task6 Package
 */
public class AirplaneModeActivity extends ParentActivity {
    Button btnBrocast, btnIntent;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dialogs);
        findViewById();
        init();
    }

    private void findViewById() {
        btnBrocast = findViewById(R.id.btnPopUpWindow);
        btnIntent = findViewById(R.id.btnDialog);
    }

    private void init() {
        btnBrocast.setText(getString(R.string.broadcast));
        btnIntent.setText(getString(R.string.intent));

        setActivityTitle(getString(R.string.airplane_activity));
        initMainBack();

        btnBrocast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BroadCastAlarm();
            }
        });

        imgMoreMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MySettingsActivity.class);
                startActivity(intent);
            }
        });

        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airplane();
            }
        });

    }

    public void BroadCastAlarm() {
        //Broadcast Receiver

    }

    public void airplane() {
        boolean isEnabled = android.provider.Settings.System.getInt(
                getContentResolver(),
                android.provider.Settings.System.AIRPLANE_MODE_ON, 0) == 1;

        // toggle airplane mode
        android.provider.Settings.System.putInt(
                getContentResolver(),
                android.provider.Settings.System.AIRPLANE_MODE_ON, isEnabled ? 0 : 1);

        // Post an intent to reload
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", !isEnabled);
        sendBroadcast(intent);
    }

}

