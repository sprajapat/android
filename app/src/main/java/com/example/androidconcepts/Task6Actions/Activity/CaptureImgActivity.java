package com.example.androidconcepts.Task6Actions.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.androidconcepts.ParentActivity;
import com.example.androidconcepts.MySettingsActivity;
import com.example.androidconcepts.R;

public class CaptureImgActivity extends ParentActivity {
    Button btnCapture;
    ImageView imgData;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final Uri locationForPhotos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_img);
        findViewById();
        init();
    }

    private void findViewById() {
        btnCapture = findViewById(R.id.btnCapture);
        imgData = findViewById(R.id.imgData);
    }

    private void init() {
        setActivityTitle(getString(R.string.alarm_activity));
        initMainBack();

        imgMoreMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent languageIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
//                startActivity(languageIntent);
                Intent intent = new Intent(getApplicationContext(), MySettingsActivity.class);
                startActivity(intent);
            }
        });

        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturePhoto("Image");
            }
        });
    }


    public void capturePhoto(String targetFilename) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.withAppendedPath(locationForPhotos, targetFilename));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) { Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgData.setImageBitmap(imageBitmap);

//            int thumbnail = data.getParcelableExtra("Image");
            // Do other work with full size photo saved in locationForPhotos
//            imgData.setImageResource(thumbnail);
        }

    }
}
