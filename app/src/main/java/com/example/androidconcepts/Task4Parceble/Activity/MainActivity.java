package com.example.androidconcepts.Task4Parceble.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task4Parceble.Model.DataObject;

public class MainActivity extends AppCompatActivity {
    Button btnSendData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_parceble);
        findViewById();
        init();
    }


    private void findViewById() {
        btnSendData = findViewById(R.id.btnSendData);
    }

    private void init() {


        final DataObject dataObject = new DataObject(1, "suraj");
        btnSendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
                intent.putExtra("Data", dataObject);
                startActivity(intent);
            }
        });


    }

}
