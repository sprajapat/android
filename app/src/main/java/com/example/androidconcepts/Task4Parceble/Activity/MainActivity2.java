package com.example.androidconcepts.Task4Parceble.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task4Parceble.Model.DataObject;

public class MainActivity2 extends AppCompatActivity {
    TextView txtName, txtId;
    DataObject dataObject;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_parceble);
        intentParam();
        findViewById();
        init();
    }

    private void intentParam() {
        Intent intent = getIntent();
        dataObject = intent.getParcelableExtra("Data");
    }


    private void findViewById() {
        txtId = findViewById(R.id.txtId);
        txtName = findViewById(R.id.txtName);
    }

    private void init() {
        txtId.setText(String.valueOf(dataObject.id));
        txtName.setText(dataObject.name);
    }
}
