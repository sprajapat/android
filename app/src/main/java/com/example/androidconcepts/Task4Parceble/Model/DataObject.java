package com.example.androidconcepts.Task4Parceble.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class DataObject implements Parcelable {
    public int id;
    public String name;


    public DataObject(int id, String name) {
        this.id = id;
        this.name = name;
    }

    protected DataObject(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<DataObject> CREATOR = new Creator<DataObject>() {
        @Override
        public DataObject createFromParcel(Parcel in) {
            return new DataObject(in);
        }

        @Override
        public DataObject[] newArray(int size) {
            return new DataObject[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    public static Creator<DataObject> getCREATOR() {
        return CREATOR;
    }
}
