package com.example.androidconcepts.Task8RenderScript.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.androidconcepts.R;
import com.example.androidconcepts.Task8RenderScript.Utilities.BlurUtils;

public class ImageActivity extends AppCompatActivity {
    ImageView imgMusic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dialogs);
        findViewById();
        init();
    }

    private void findViewById() {
        imgMusic = findViewById(R.id.imgMusic);
    }

    private void init() {
        Bitmap bitmap = ((BitmapDrawable) imgMusic.getDrawable()).getBitmap();
        imgMusic.setImageBitmap(new BlurUtils().blur(getApplicationContext(), bitmap, 10.5f));
    }
}