package com.example.androidconcepts;

import android.content.Context;
import android.widget.Toast;

public class Utils {

    public static String nullSafe(String strValue) {
        return strValue == null || strValue.trim().length() <= 0 || strValue.equalsIgnoreCase("null") ? "" : strValue;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
