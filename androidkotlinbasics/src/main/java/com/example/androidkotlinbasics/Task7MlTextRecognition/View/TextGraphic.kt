package com.example.androidkotlinbasics.Task7MlTextRecognition.View

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.Log
import com.example.androidkotlinbasics.Task7MlTextRecognition.View.GraphicOverlay
import com.google.firebase.ml.vision.text.FirebaseVisionText

class TextGraphic(overlay: GraphicOverlay) : GraphicOverlay.Graphic(overlay) {
    private val TAG = "TextGraphic"
    private val RECT_COLOR = Color.YELLOW
    private val TEXT_COLOR = Color.BLUE
    private val TEXT_SIZE = 24.0f
    private val STROKE_WIDTH = 4.0f

    private lateinit var rectPaint: Paint
    private lateinit var textPaint: Paint
    private var element: FirebaseVisionText.Element? = null

    constructor(overlay: GraphicOverlay, element: FirebaseVisionText.Element?) : this(overlay) {
        this.element = element
        rectPaint = Paint()
        rectPaint.setColor(RECT_COLOR)
        rectPaint.setStyle(Paint.Style.STROKE)
        rectPaint.setStrokeWidth(STROKE_WIDTH)
        textPaint = Paint()
        textPaint.setColor(TEXT_COLOR)
        textPaint.setTextSize(TEXT_SIZE)
        // Redraw the overlay, as this graphic has been added.
        postInvalidate()
    }

    override fun draw(canvas: Canvas?) {
        Log.d(TAG, "on draw text graphic")
        checkNotNull(element) { "Attempting to draw a null text." }

        val rect = RectF(element!!.getBoundingBox())
        canvas!!.drawRect(rect, rectPaint)

        canvas.drawText(element!!.getText(), rect.left, rect.bottom, textPaint)
    }
}