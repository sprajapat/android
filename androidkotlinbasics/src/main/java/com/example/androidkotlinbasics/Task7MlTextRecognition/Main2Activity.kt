package com.example.androidkotlinbasics.Task7MlTextRecognition

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.Surface
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.LifecycleOwner
import com.bumptech.glide.GenericTransitionOptions.with
import com.bumptech.glide.Glide.with
import com.example.androidkotlinbasics.CommanUtils.showToast
import com.example.androidkotlinbasics.CommanUtils.visibilityGone
import com.example.androidkotlinbasics.CommanUtils.visible
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task7MlTextRecognition.View.GraphicOverlay
import com.example.androidkotlinbasics.Task7MlTextRecognition.View.TextGraphic
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main2.*
import java.io.File


class Main2Activity : AppCompatActivity() {
    lateinit var bitmap: Bitmap
    private val REQUEST_CODE_PERMISSIONS = 10
    private val IMAGE_PICK = 1
    private val REQUIRED_PERMISSIONS = arrayOf("android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        init()
    }

    private fun init() {
        imgView.setImageResource(R.drawable.a)
//        viewFinder.visibilityGone()
        btnText.setOnClickListener {
            imgView.visible()
            viewFinder.visibilityGone()
            mGraphicOverlay.visible()

            mGraphicOverlay.clear()
            runTextRecognition()
        }

        btnImg.setOnClickListener {

            if (allPermissionsGranted()) {
                startCamera() //start camera if permission has been granted by user
            } else {
                ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
            }
        }

        btnImg2.setOnClickListener {
            viewFinder.visibilityGone()
            var chooseFile = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            chooseFile.type = "image/*"
            startActivityForResult(Intent.createChooser(chooseFile, "Select Picture"), IMAGE_PICK)
        }


    }

    private fun runTextRecognition() {
//        viewFinder.visibilityGone()
        bitmap = (imgView.getDrawable() as BitmapDrawable).toBitmap()
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        Log.e("inside ", "runTextRecognition")
        val recognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
        recognizer.processImage(image)
                .addOnSuccessListener { texts ->
                    processTextRecognitionResult(texts)
                }
                .addOnFailureListener { e -> // Task failed with an exception
                    e.printStackTrace()
                }
    }

    private fun processTextRecognitionResult(texts: FirebaseVisionText) {
        Log.e("Text :", texts.toString())
        val blocks = texts.textBlocks
        if (blocks.size == 0) {
            showToast("No text found")
            return
        }

        mGraphicOverlay.clear()
        for (i in blocks.indices) {
            val lines = blocks[i].lines
            for (j in lines.indices) {
                val elements = lines[j].elements
                for (k in elements.indices) {
                    val textGraphic: GraphicOverlay.Graphic = TextGraphic(mGraphicOverlay, elements[k])
                    mGraphicOverlay.add(textGraphic)
                }
            }
        }
    }

    private fun startCamera() {
        viewFinder.visible()
        imgView.visibilityGone()
        mGraphicOverlay.visibilityGone()
        CameraX.unbindAll()
        val aspectRatio = Rational(viewFinder.width, viewFinder.height)
        val screen = Size(viewFinder.width, viewFinder.height) //size of the screen
        val pConfig = PreviewConfig.Builder().setTargetAspectRatio(aspectRatio).setTargetResolution(screen).build()
        val preview = Preview(pConfig)

        preview.onPreviewOutputUpdateListener = Preview.OnPreviewOutputUpdateListener { output ->
            //to update the surface texture we  have to destroy it first then re-add it
            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)
            viewFinder.setSurfaceTexture(output.surfaceTexture)
            updateTransform()
        }

        val imageCaptureConfig = ImageCaptureConfig.Builder().setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(windowManager.defaultDisplay.rotation).build()
        val imgCap = ImageCapture(imageCaptureConfig)

        viewFinder.setOnClickListener {
            val file = File(Environment.getExternalStorageDirectory().toString() + "/" + System.currentTimeMillis() + ".png")
            imgCap.takePicture(file, object : ImageCapture.OnImageSavedListener {
                override fun onImageSaved(file: File) {
                    val msg = "Pic captured at " + file.absolutePath
                    Picasso.get().load(file).into(imgView)
                    imgView.visible()
                    viewFinder.visibilityGone()
                    mGraphicOverlay.visible()

//                    val ImageUri: Uri = Uri.fromFile(file)
//                    bitmap = MediaStore.Images.Media.getBitmap(contentResolver, ImageUri);
//                    imgView.setImageBitmap(bitmap)
                    Toast.makeText(baseContext, msg, Toast.LENGTH_LONG).show()
                }

                override fun onError(useCaseError: ImageCapture.UseCaseError, message: String, cause: Throwable?) {
                    val msg = "Pic capture failed : $message"
                    Toast.makeText(baseContext, msg, Toast.LENGTH_LONG).show()
                    cause?.printStackTrace()
                }
            })
        }
        //bind to lifecycle:
        CameraX.bindToLifecycle(this as LifecycleOwner, preview, imgCap)
    }

    private fun updateTransform() {
        var mx = Matrix()
        var w: Float = viewFinder.measuredWidth.toFloat()
        var h: Float = viewFinder.measuredHeight.toFloat()
        var cX = w / 2f
        var cY = h / 2f
        var rotationDgr: Int
        var rotat: Int = viewFinder.rotation.toInt()

        rotationDgr = when (rotat) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        mx.postRotate(rotationDgr.toFloat(), cX, cY)
        viewFinder.setTransform(mx)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, datas: Intent?) {
        super.onActivityResult(requestCode, resultCode, datas)
        if (requestCode == IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val uri: Uri? = datas?.data
            var bitmap: Bitmap?
//            bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
//            imgView.setImageBitmap(bitmap)
            Picasso.get().load(uri).into(imgView)
            imgView.visible()
            viewFinder.visibilityGone()
            mGraphicOverlay.visible()

            Log.e("success", "Bundle Success")
        } else {
            Log.e("Failed", "to Load")
        }
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
