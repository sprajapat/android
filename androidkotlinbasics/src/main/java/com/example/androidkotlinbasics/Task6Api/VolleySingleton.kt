package com.example.androidkotlinbasics.Task6Api

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

open class VolleySingleton {
    private var mInstance: VolleySingleton? = null
    var context: Context? = null
    private var mRequestQueue: RequestQueue? = null


    fun Context.VolleySingleton() {
        context = this
        mRequestQueue = getRequestQueue()
    }

    @Synchronized
    fun getInstance(context: Context): VolleySingleton? {
        if (mInstance == null) mInstance = VolleySingleton()
        return mInstance
    }

    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context?.applicationContext)
        }
        return mRequestQueue
    }


    fun <T> addToRequestQueue(req: Request<T>?) {
        getRequestQueue()?.add(req)
    }

    fun cancelPendingRequests(tag: Any?) {
        mRequestQueue?.cancelAll(tag)
    }


}