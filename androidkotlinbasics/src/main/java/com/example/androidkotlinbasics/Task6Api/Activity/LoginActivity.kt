package com.example.androidkotlinbasics.Task6Api.Activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.androidkotlinbasics.CommanUtils.*
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task6Api.VolleySingleton
import com.example.androidkotlinbasics.Task6Api.VollyService
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init();
    }

    private fun init() {
        llLogin.setOnClickListener {
            progressBar.setVisibility(View.VISIBLE)
            userLogin()
        }
    }

    private fun userLogin() {
        if (isConnected()) {

            val currentTimeStemp: String? = getCurrentTimeStemp()
            ANDROID_DEVICE_ID = "1234"
            var stringMap = HashMap<String, String>()
            stringMap.put(DEVICE_ID, ANDROID_DEVICE_ID)
            stringMap.put(DEVICE_TYPE, "android")
            stringMap.put(EMAIl, edtEmail.text.toString())
            stringMap.put(PASSWORD, edtPassword.text.toString())
            Log.e("login response", " >> $stringMap")

            val jString = JSONObject(stringMap as Map<*, *>)

            val signature: String = encodeSha56(currentTimeStemp + POST + login.toString() + jString.toString())

            var headerMap = HashMap<String, String>()
            headerMap.put(Authorization, getUserToken(applicationContext))
            headerMap.put(API_KEY, API_KEY_VALUE)
            headerMap.put(API_TIMESTAMP, currentTimeStemp!!)
            headerMap.put(API_SECRET, signature)

            var jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST,
                    URL_LOGIN, jString, Response.Listener { response ->

                val str = response.toString()
                Log.e("login response", " >> $response")
                try {
                    val msg = response.getString("msg")
                    Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                progressBar.visibility = View.GONE

//               startActivity(Intent(applicationContext, ProfileActivity::class.java))
            }, Response.ErrorListener { error ->
                progressBar.visibility = View.GONE
                error.printStackTrace()
                Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
            }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return headerMap
                }
            }
            VollyService.initialize(applicationContext)
            VollyService.requestQueue.add(jsonObjectRequest)
            VollyService.requestQueue.start()
//
//            val volleySingleton: VolleySingleton? = VolleySingleton().getInstance(applicationContext)
//            volleySingleton?.addToRequestQueue(jsonObjectRequest)

        } else {
            showToast(getString(R.string.Internet_connectivity))
        }
    }

    fun isConnected(): Boolean {
        var flag = false
        var connManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mMobileNewtork = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (mWifi.isConnected || mMobileNewtork.isConnected) { // Do whatever
            Toast.makeText(applicationContext, "Connected ", Toast.LENGTH_SHORT).show();
            flag = true
        }
        return flag
    }
}


