package com.example.androidkotlinbasics.Task6Api

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

object VollyService {
    private lateinit var context: Context

    val requestQueue: RequestQueue by lazy { Volley.newRequestQueue(context) }

    fun initialize(context: Context) {
        this.context = context.applicationContext
    }
}