package com.example.androidkotlinbasics.Task4ViewPager
/*******************************
author suraj
dated - 29feb,2020
Task- ViewPager & Recycler
content
1.PagerActivity
2.MainAdapter
3.DataObject class
4.UtilPackage
 ********************************/
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task3Recycler.model.DataObject

class MainPager(val context: Context /*, val dataList: ArrayList<DataObject>*/) : PagerAdapter() {
//
    var dataList = ArrayList<DataObject>()
//
    fun setData(dataList: ArrayList<DataObject>) {
        this.dataList = dataList
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun getCount() = dataList.size

    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.row_pager, parent, false)

        val imageView: ImageView = view.findViewById(R.id.imgView1)

        imageView.setImageResource(dataList[position].id)
        parent?.addView(view)
        return view
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        parent.removeView(`object` as View)
    }
}