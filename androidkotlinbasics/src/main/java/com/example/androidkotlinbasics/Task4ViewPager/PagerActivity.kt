package com.example.androidkotlinbasics.Task4ViewPager

/*******************************
author suraj
dated - 29feb,2020
Task- ViewPager & Recycler
content
1.PagerActivity
2.MainAdapter
3.DataObject class
4.UtilPackage
 ********************************/
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.androidkotlinbasics.CommanUtils.visible
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task3Recycler.Adapter.ParentAdapter
import com.example.androidkotlinbasics.Task3Recycler.model.DataObject

class PagerActivity : AppCompatActivity() {
    val dataList2 = ArrayList<DataObject>()
    val dataList = ArrayList<DataObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_pager)
        Toast.makeText(applicationContext, "You are changing orientation...", Toast.LENGTH_SHORT).show();

        init()
    }

    private fun init() {
        val viewPager = findViewById(R.id.viewPager) as ViewPager
        viewPager.visible()
        val recyclerView = findViewById(R.id.recylrView) as RecyclerView

        //viewpager adapter set
        dataList.add(DataObject(R.drawable.img1))
        dataList.add(DataObject(R.drawable.img2))
        dataList.add(DataObject(R.drawable.img3))
        var mainPager = MainPager(applicationContext/*, dataList*/)
        mainPager.setData(dataList)
        viewPager.setOffscreenPageLimit(5);
//        viewPager.pageMargin = resources.getDimensionPixelOffset(R.dimen.dp_64)
        viewPager.setAdapter(mainPager)

        //recycler adaper set
        var linearLayoutManager: LinearLayoutManager
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        val adapter = ParentAdapter(applicationContext)
        recyclerView.adapter = adapter
        adapter.setData(dataList2)

        //now adding the adapter to recyclerview
        //adding some dummy data to the list
        dataList2.add(DataObject(1, "ABC"))
        dataList2.add(DataObject(2, "CDE"))
        dataList2.add(DataObject(3, "DEF"))
        dataList2.add(DataObject(4, "FGH"))
        dataList2.add(DataObject(5, "FGH"))
        dataList2.add(DataObject(6, "FGH"))
        dataList2.add(DataObject(7, "FGH"))
        dataList2.add(DataObject(8, "FGH"))
        Log.e("DataList", dataList2.toString())

    }
}
