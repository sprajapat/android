package com.example.androidkotlinbasics.Task8FaceRecognition.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.Surface
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.androidkotlinbasics.CommanUtils.visibilityGone
import com.example.androidkotlinbasics.CommanUtils.visible
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task7MlTextRecognition.View.GraphicOverlay
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_face_recognition.*
import java.io.File
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

class FaceRecognitionActivity : AppCompatActivity() {
    lateinit var bitmap: Bitmap
    private val REQUEST_CODE_PERMISSIONS = 10
    private val IMAGE_PICK = 1
    private val CAMERA_IMAGE = 2
    private val REQUIRED_PERMISSIONS = arrayOf("android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE")
    private lateinit var outputFileUri: Uri
    var lensFacing: CameraX.LensFacing = CameraX.LensFacing.BACK

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_face_recognition)
        inti()
    }

    private fun inti() {

        btnImg.setOnClickListener {

            if (allPermissionsGranted()) {
                startCamera(lensFacing) //start camera if permission has been granted by user
            } else {
                ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
            }
        }

        btnImg2.setOnClickListener {
            viewFinder.visibilityGone()
            var chooseFile = Intent(Intent.ACTION_GET_CONTENT/*, MediaStore.Images.Media.ACTION_GET_CONTENT*/)
            chooseFile.type = "image/*"
            startActivityForResult(Intent.createChooser(chooseFile, "Select Picture"), IMAGE_PICK)
        }
        btnImg3.setOnClickListener {
            val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePhotoIntent.resolveActivity(packageManager) != null) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.TITLE, "MLKit_codelab")
                outputFileUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!

                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                startActivityForResult(takePhotoIntent, CAMERA_IMAGE)
            }
        }
        btnSwap.setOnClickListener {
            swapCamera()
            startCamera(lensFacing)
        }
    }

    private fun startCamera(lensFacing: CameraX.LensFacing) {

        viewFinder.visible()
        imgView.visibilityGone()
        CameraX.unbindAll()
//        var cameraOrientation = if (cameraOrientation === CameraX.LensFacing.BACK) CameraX.LensFacing.FRONT else CameraX.LensFacing.BACK
        val aspectRatio = Rational(viewFinder.width, viewFinder.height)
        val screen = Size(viewFinder.width, viewFinder.height) //size of the screen

        val pConfig = PreviewConfig.Builder().setLensFacing(lensFacing)
                .setTargetAspectRatio(aspectRatio).setTargetResolution(screen).build()
        val preview = Preview(pConfig)

        preview.onPreviewOutputUpdateListener = Preview.OnPreviewOutputUpdateListener { output ->
            //to update the surface texture we  have to destroy it first then re-add it
            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)
            viewFinder.setSurfaceTexture(output.surfaceTexture)
            updateTransform()
        }

//        val imageCaptureConfig = ImageCaptureConfig.Builder().setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
//                .setTargetRotation(windowManager.defaultDisplay.rotation).build()

        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            setLensFacing(lensFacing)
            setTargetRotation(windowManager.defaultDisplay.rotation)
        }.build()

        val imgCap = ImageCapture(imageCaptureConfig)
        btnSwap.setOnClickListener {
            swapCamera()
        }

        viewFinder.setOnClickListener {
            val file = File(Environment.getExternalStorageDirectory().toString() + "/" + System.currentTimeMillis() + ".png")

            imgCap.takePicture(file, object : ImageCapture.OnImageSavedListener {
                override fun onImageSaved(file: File) {
                    val msg = "Pic captured at " + file.absolutePath
                    Picasso.get().load(file).into(imgView)
                    imgView.visible()
                    viewFinder.visibilityGone()
                    Toast.makeText(baseContext, msg, Toast.LENGTH_LONG).show()
//                    runObjectDetection()
                }

                override fun onError(useCaseError: ImageCapture.UseCaseError, message: String, cause: Throwable?) {
                    val msg = "Pic capture failed : $message"
                    Toast.makeText(baseContext, msg, Toast.LENGTH_LONG).show()
                    cause?.printStackTrace()
                }
            })
        }

        /*
            val analyzerConfig = ImageAnalysisConfig.Builder().apply {
                setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
            }.build()
            // Build the image analysis use case and instantiate our analyzer
            val analyzerUseCase = ImageAnalysis(analyzerConfig).apply {
                analyzer = LuminosityAnalyzer()
            }*/
//        bind to lifecycle:
        CameraX.bindToLifecycle(this as LifecycleOwner, imgCap, preview/*, analyzerUseCase*/)
    }

    @SuppressLint("RestrictedApi")
    private fun swapCamera() {
        var lensFacing2: String? = CameraX.getCameraWithLensFacing(lensFacing)
        if (lensFacing2 == "0") {
            lensFacing = CameraX.LensFacing.FRONT
            Log.e("camera Front", lensFacing.toString())
        } else {
            this.lensFacing = CameraX.LensFacing.BACK
            Log.e("camera BACK", lensFacing.toString())
        }

        Log.e("camera type", lensFacing2)
        startCamera(lensFacing)
//        CameraX.getCameraWithLensFacing(lensFacing)
    }

    private fun updateTransform() {
        var mx = Matrix()
        var w: Float = viewFinder.measuredWidth.toFloat()
        var h: Float = viewFinder.measuredHeight.toFloat()
        var cX = w / 2f
        var cY = h / 2f
        var rotationDgr: Int
        var rotat: Int = viewFinder.rotation.toInt()

        rotationDgr = when (rotat) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        mx.postRotate(rotationDgr.toFloat(), cX, cY)
        viewFinder.setTransform(mx)
    }

    private class LuminosityAnalyzer : ImageAnalysis.Analyzer {
        private var lastAnalyzedTimestamp = 0L

        /**
         * Helper extension function used to extract a byte array from an
         * image plane buffer
         */
        private fun ByteBuffer.toByteArray(): ByteArray {
            rewind()    // Rewind the buffer to zero
            val data = ByteArray(remaining())
            get(data)   // Copy the buffer into a byte array
            return data // Return the byte array
        }

        override fun analyze(image: ImageProxy, rotationDegrees: Int) {
            val currentTimestamp = System.currentTimeMillis()
            // Calculate the average luma no more often than every second
            if (currentTimestamp - lastAnalyzedTimestamp >=
                    TimeUnit.SECONDS.toMillis(1)) {
                // Since format in ImageAnalysis is YUV, image.planes[0]
                // contains the Y (luminance) plane
                val buffer = image.planes[0].buffer
                // Extract image data from callback object
                val data = buffer.toByteArray()
                // Convert the data into an array of pixel values
                val pixels = data.map { it.toInt() and 0xFF }
                // Compute average luminance for the image
                val luma = pixels.average()
                // Log the new luma value
                Log.d("CameraXApp", "Average luminosity: $luma")
                // Update timestamp of last analyzed frame
                lastAnalyzedTimestamp = currentTimestamp
            }
        }
    }

    private fun potraitMode(/*imgView: ImageView*/) {

        var mx = Matrix()
        var w: Float = imgView.measuredWidth.toFloat()
        var h: Float = imgView.measuredHeight.toFloat()

        var cX = w / 2f
        var cY = h / 2f
        var rotationDgr: Int

        var rotat: Int = resources.configuration.orientation as Int

        rotationDgr = when (rotat) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 0
            Surface.ROTATION_180 -> 0
            Surface.ROTATION_270 -> 0
            else -> return
        }
        mx.postRotate(rotationDgr.toFloat(), cX, cY)
        imgView.imageMatrix = mx

    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, datas: Intent?) {
        super.onActivityResult(requestCode, resultCode, datas)
        if (requestCode == IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val uri: Uri? = datas?.data
            bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
            Picasso.get().load(uri).into(imgView)
//            imgView.rotation = 90F
//            imgView.rotation = potraitMode()
            potraitMode(/*imgView*/)
            imgView.visible()
            viewFinder.visibilityGone()
            runObjectDetection(bitmap)
        } else if (requestCode == CAMERA_IMAGE &&
                resultCode == Activity.RESULT_OK) {
            val image = getCapturedImage()
            imgView.setImageBitmap(image)
            runObjectDetection(image)
        }
    }

    private fun getCapturedImage(): Bitmap {
        val srcImage = FirebaseVisionImage.fromFilePath(baseContext, outputFileUri).bitmap

        // crop image to match imageView's aspect ratio
        val scaleFactor = Math.min(
                srcImage.width / imgView.width.toFloat(),
                srcImage.height / imgView.height.toFloat()
        )

        val deltaWidth = (srcImage.width - imgView.width * scaleFactor).toInt()
        val deltaHeight = (srcImage.height - imgView.height * scaleFactor).toInt()

        val scaledImage = Bitmap.createBitmap(
                srcImage, deltaWidth / 2, deltaHeight / 2,
                srcImage.width - deltaWidth, srcImage.height - deltaHeight
        )
        srcImage.recycle()
        return scaledImage

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera(lensFacing)
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }

    }

    private fun runObjectDetection(bitmap: Bitmap) {

        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val options = FirebaseVisionFaceDetectorOptions.Builder()
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .setMinFaceSize(0.15f)
                .enableTracking()
                .build()

        val detector = FirebaseVision.getInstance().getVisionFaceDetector(options)

        val result = detector.detectInImage(image).addOnSuccessListener { faces ->
            for (face in faces) {
                val bounds = face.boundingBox
                val rotY = face.headEulerAngleY // Head is rotated to the right rotY degrees
                val rotZ = face.headEulerAngleZ // Head is tilted sideways rotZ degrees

                val leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR)
                leftEar?.let {
                    val leftEarPos = leftEar.position
                    Log.e("Happiness", leftEar.toString())
                }
                Log.e("Okk", "oKk")
                // If classification was enabled:
                if (face.smilingProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                    val smileProb = face.smilingProbability
                }
                if (face.rightEyeOpenProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                    val rightEyeOpenProb = face.rightEyeOpenProbability
                }

                // If face tracking was enabled:
                if (face.trackingId != FirebaseVisionFace.INVALID_ID) {
                    val id = face.trackingId
                }
            }
            processFaceRecognitionResult(faces)
        }.addOnFailureListener {
            Toast.makeText(
                    baseContext, "Oops, something went wrong!",
                    Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun processFaceRecognitionResult(faces: MutableList<FirebaseVisionFace>) {
        imgView.visible()
        viewFinder.visibilityGone()
        mgraphicOverlay.visible()

        mgraphicOverlay.clear()
        for (i in faces.indices) {
            val face = faces[i]
//            val cameraFacing = frameMetadata.cameraFacing
            val faceGraphic = FaceGraphic(mgraphicOverlay, face)
            mgraphicOverlay.add(faceGraphic)
//
//            mgraphicOverlay.postInvalidate()
        }
    }

    class FaceGraphic(overlay: GraphicOverlay, private val firebaseVisionFace: FirebaseVisionFace?) : GraphicOverlay.Graphic(overlay) {
        private val facePositionPaint = Paint().apply {
            color = Color.WHITE
        }

        private val idPaint = Paint().apply {
            color = Color.YELLOW
            textSize = ID_TEXT_SIZE
        }

        private val boxPaint = Paint().apply {
            color = Color.WHITE
            style = Paint.Style.STROKE
            strokeWidth = BOX_STROKE_WIDTH
        }

        override fun draw(canvas: Canvas?) {
            val face = firebaseVisionFace ?: return

            // Draws a circle at the position of the detected face, with the face's track id below.
            // An offset is used on the Y axis in order to draw the circle, face id and happiness level in the top area
            // of the face's bounding box
            val x = translateX(face.boundingBox.centerX().toFloat())
            val y = translateY(face.boundingBox.centerY().toFloat())
            canvas!!.drawCircle(x, y - 4 * ID_Y_OFFSET, FACE_POSITION_RADIUS, facePositionPaint)
            canvas.drawText("id: " + face.trackingId, x + ID_X_OFFSET, y - 3 * ID_Y_OFFSET, idPaint)
            canvas.drawText(
                    "happiness: ${String.format("%.2f", face.smilingProbability)}", x + ID_X_OFFSET * 3, y - 2 * ID_Y_OFFSET, idPaint)

            /*  if (face == CameraSource.CAMERA_FACING_FRONT) {*/
            canvas.drawText(
                    "right eye: ${String.format("%.2f", face.rightEyeOpenProbability)}",
                    x - ID_X_OFFSET,
                    y,
                    idPaint
            )
            canvas.drawText(
                    "left eye: ${String.format("%.2f", face.leftEyeOpenProbability)}",
                    x + ID_X_OFFSET * 6,
                    y,
                    idPaint
            )
            /*  } else {
                  canvas.drawText(
                          "left eye: ${String.format("%.2f", face.leftEyeOpenProbability)}",
                          x - ID_X_OFFSET,
                          y,
                          idPaint
                  )
                  canvas.drawText(
                          "right eye: ${String.format("%.2f", face.rightEyeOpenProbability)}",
                          x + ID_X_OFFSET * 6,
                          y,
                          idPaint
                  )
              }*/

            // Draws a bounding box around the face.
            val xOffset = scaleX(face.boundingBox.width() / 2.0f)
            val yOffset = scaleY(face.boundingBox.height() / 2.0f)
            val left = x - xOffset
            val top = y - yOffset
            val right = x + xOffset
            val bottom = y + yOffset
            canvas.drawRect(left, top, right, bottom, boxPaint)

            // draw landmarks
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.MOUTH_BOTTOM)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_CHEEK)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_EAR)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.MOUTH_LEFT)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_EYE)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_CHEEK)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_EAR)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_EYE)
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.MOUTH_RIGHT)
        }

        private fun drawLandmarkPosition(canvas: Canvas, face: FirebaseVisionFace, landmarkID: Int) {
            val landmark = face.getLandmark(landmarkID)
            landmark?.let {
                val point = it.position
                canvas.drawCircle(
                        translateX(point.x),
                        translateY(point.y),
                        10f, idPaint
                )
            }
        }

        /*   private fun drawBitmapOverLandmarkPosition(canvas: Canvas, face: FirebaseVisionFace, landmarkID: Int) {
               val landmark = face.getLandmark(landmarkID) ?: return

               val point = landmark.position

               overlayBitmap?.let {
                   val imageEdgeSizeBasedOnFaceSize = face.boundingBox.width() / 4.0f

                   val left = (translateX(point.x) - imageEdgeSizeBasedOnFaceSize).toInt()
                   val top = (translateY(point.y) - imageEdgeSizeBasedOnFaceSize).toInt()
                   val right = (translateX(point.x) + imageEdgeSizeBasedOnFaceSize).toInt()
                   val bottom = (translateY(point.y) + imageEdgeSizeBasedOnFaceSize).toInt()

                   canvas.drawBitmap(it, null,
                           Rect(left, top, right, bottom), null)
               }
           }*/

        companion object {
            private const val FACE_POSITION_RADIUS = 4.0f
            private const val ID_TEXT_SIZE = 30.0f
            private const val ID_Y_OFFSET = 50.0f
            private const val ID_X_OFFSET = -50.0f
            private const val BOX_STROKE_WIDTH = 5.0f
        }
    }


}
