package com.example.androidkotlinbasics.Task1Intent.Utils

import android.content.Context
import android.widget.ImageView
import android.widget.Toast

object Utils {

    fun showToast(context: Context, msg : String){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    fun displayaImage(context: Context, url : String, imgview : ImageView){
        imgview.setImageDrawable(null)
    }
}