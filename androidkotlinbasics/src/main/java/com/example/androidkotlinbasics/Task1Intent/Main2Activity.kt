package com.example.androidkotlinbasics.Task1Intent
/*******************************
author suraj
dated - 29feb,2020
Task- pass intent data to othter activity
content
1.MainActivity
2.MainActivity2
3.UtilPackage
********************************/

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task1Intent.Utils.Constants
import kotlinx.android.synthetic.main.activity_intent_main2.*
import kotlinx.android.synthetic.main.activity_intent_main2.txtTitle


class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_main2)
        init()
    }

    private fun init() {
        if (intent.extras != null) {
            txtTitle.text = intent.getStringExtra(Constants.MainTxt1)
            txtTitle2.text = intent.getIntExtra(Constants.MainTxt2,0).toString()
            imgView1.setImageResource(intent.getIntExtra(Constants.MainImg1, 0))
        }
    }

}

