package com.example.androidkotlinbasics.Task1Intent

/*author suraj
dated - 29feb,2020
Task- pass intent data to othter activity
content
1.MainActivity
2.MainActivity2
3.UtilPackage
*/

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task1Intent.Utils.Constants
import com.example.androidkotlinbasics.Task1Intent.Utils.Utils
import kotlinx.android.synthetic.main.activity_intent_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_main)
        init()
    }

    private fun init() {

        txtTitle.text = "Main Activity"
        btn1.setOnClickListener {
            Utils.showToast(this@MainActivity, "sdfjdshlfijsh")
            txtTitle2.text = txtTitle.text
            var intent = Intent(applicationContext, Main2Activity::class.java)
            intent.putExtra(Constants.MainTxt1, txtTitle.text.toString())
            intent.putExtra(Constants.MainImg1, R.drawable.ic_launcher_foreground)
            intent.putExtra(Constants.MainTxt2, 3)
            startActivity(intent)
        }
    }
}