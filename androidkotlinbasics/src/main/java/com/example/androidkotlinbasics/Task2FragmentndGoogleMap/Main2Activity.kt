package com.example.androidkotlinbasics.Task2FragmentndGoogleMap

/*******************************
author suraj
dated - 29feb,2020
Task- fragment load & google map in fragment
content
1.MainActivity
2.NewFragment
3.UtilPackage
 ********************************/
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.androidkotlinbasics.R

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_main2)
        init()
    }

    private fun init() {
        val newFragment = NewFragment()
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.replace(R.id.llContainer, newFragment)
        transaction.commit()
    }

}

