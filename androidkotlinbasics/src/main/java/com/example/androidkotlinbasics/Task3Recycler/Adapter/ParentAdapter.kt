package com.example.androidkotlinbasics.Task3Recycler.Adapter
/*******************************
author suraj
dated - 29feb,2020
Task- RecyclerView
content
1.RecyclerActivity
2.ParentAdapter
3.DataObject class
4.UtilPackage
 ********************************/
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task3Recycler.model.DataObject
import kotlinx.android.synthetic.main.row_adapter.view.*

class ParentAdapter(context: Context) : RecyclerView.Adapter<ParentAdapter.MyViewHolder>() {

    var dataList = ArrayList<DataObject>()

    fun setData(dataList: ArrayList<DataObject>) {
        this.dataList = dataList
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userName = view.txtName
        val userId = view.txtId

//        fun bind(data: DataObject) {
//            data = dataList
//
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MyViewHolder(LayoutInflater.from(parent?.context)
                    .inflate(R.layout.row_adapter, parent, false))

    override fun getItemCount() = dataList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        holder.bind(dataList[position])
        var data: DataObject = dataList[position]
        holder.userName.text = data.name
        holder.userId.text = data.id.toString()
    }

}