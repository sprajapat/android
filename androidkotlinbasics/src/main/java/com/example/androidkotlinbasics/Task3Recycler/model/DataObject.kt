package com.example.androidkotlinbasics.Task3Recycler.model

class DataObject {
    lateinit var imgId: Unit
    var id: Int = 0
    var name: String? = null
    var value: String? = null
    var isSelected = false
    var subDataObjects: List<SubDataObject>? = null

    constructor() {}

    constructor(id: Int, name: String?, value: String?) {
        this.id = id
        this.name = name
        this.value = value
    }

    constructor(id: Int, name: String?, subDataObjects: List<SubDataObject>?) {
        this.id = id
        this.name = name
        this.subDataObjects = subDataObjects
    }

    constructor(id: Int, name: String?) {
        this.id = id
        this.name = name
    }

    constructor(id: Int) {
        this.id = id
    }

    constructor(name: String?) {
        this.name = name
    }


    constructor(imgId: Unit) {
        this.imgId = imgId
    }

    class SubDataObject(var id: Int, var name: String) {
        var value: String? = null

    }
}


//class DataObject() {
//    var id: Int = 0
//    var name: String? = null
//
//    constructor(id: Int, name: String) : this()
//    constructor(id: Int) : this()
//
//}