package com.example.androidkotlinbasics.Task3Recycler.Activity

/*******************************
author suraj
dated - 29feb,2020
Task- RecyclerView
content
1.RecyclerActivity
2.RecyclerAdapterTwoAdapter
3.DataObject class
4.UtilPackage
 ********************************/
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task3Recycler.Adapter.ParentAdapter
import com.example.androidkotlinbasics.Task3Recycler.Adapter.RecyclerAdapterTwo
import com.example.androidkotlinbasics.Task3Recycler.model.DataObject
import kotlinx.android.synthetic.main.row_pager.*

class RecyclerActivity : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_pager)
        init()
    }

    private fun init() {

        val recyclerView = findViewById(R.id.recylrView) as RecyclerView
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
//        val adapter = ParentAdapter(applicationContext)
        val adapter = RecyclerAdapterTwo(applicationContext)
        recyclerView.adapter = adapter
        adapter.setData(imageList())
//        adapter.setData(dataList())

    }

    fun dataList(): ArrayList<DataObject> {

        val dataList = ArrayList<DataObject>()
        dataList.add(DataObject(1, "ABC"))
        dataList.add(DataObject(2, "CDE"))
        dataList.add(DataObject(3, "DEF"))
        dataList.add(DataObject(4, "FGH"))
        return dataList
    }

    private fun imageList(): ArrayList<DataObject> {
        val imageList = ArrayList<DataObject>()
        var i: Int = 0
        while (i < 40) {
            imageList.add(DataObject("https://upload.wikimedia.org/wikipedia/en/6/6b/Hello_Web_Series_%28Wordmark%29_Logo.png"))
            imageList.add(DataObject("http://healthyceleb.com/wp-content/uploads/2015/03/Michael-Jordan.jpg"))
            imageList.add(DataObject("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Jordan_Lipofsky.jpg/170px-Jordan_Lipofsky.jpg"))
            i++
        }
        return imageList
    }
}

