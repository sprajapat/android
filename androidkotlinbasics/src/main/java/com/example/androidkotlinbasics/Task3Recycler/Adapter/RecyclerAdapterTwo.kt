package com.example.androidkotlinbasics.Task3Recycler.Adapter

/*******************************
author suraj
dated - 3March,2020
Task- RecyclerView
content
1.RecyclerActivity
2.RecyclerAdapter2
3.DataObject class
4.UtilPackage
 ********************************/
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task3Recycler.model.DataObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_pager.view.*

class RecyclerAdapterTwo(private val context: Context) : RecyclerView.Adapter<RecyclerAdapterTwo.MyViewHolder>() {

    var dataList = ArrayList<DataObject>()

    fun setData(dataList: ArrayList<DataObject>) {
        this.dataList = dataList
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userImage: ImageView = view.imgView1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MyViewHolder(LayoutInflater.from(parent?.context)
                    .inflate(R.layout.row_pager, parent, false))

    override fun getItemCount() = dataList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var data: DataObject = dataList[position]
//        holder.userImage.setImageResource(data.id)

            Glide.with(context)
                    .load(data.name)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.userImage)

//        Picasso.get().load("${data.name}")/*.resize(100, 100)*/.into(holder.userImage)
    }
}