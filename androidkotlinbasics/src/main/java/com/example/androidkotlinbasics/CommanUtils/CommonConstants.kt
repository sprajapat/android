package com.example.androidkotlinbasics.CommanUtils

object CommonConstants {
    public var ANDROID_DEVICE_ID = "test"
    val ROOT_URL = "http://ostaad.stage02.obdemo.com/api/v1/"
    val ROOT_URL2 = "http://ostaad.stage02.obdemo.com/"
    val verify_verification_code: String = ROOT_URL + "verify-verification-code"

    val android = "android"
    val URL_SIGNUP: String = ROOT_URL + "signup"
    val URL_VERIFY: String = ROOT_URL + "verify-verification-code"
    val URL_LOGIN: String = ROOT_URL + "login"
    val POST = "POST"
    val Authorization = "Authorization"
    val loginUserToken = "loginUserToken"

    val ENCODE_BASE_URL = "api/v1/"
    val singup = ENCODE_BASE_URL + "signup"
    val login = ENCODE_BASE_URL + "login"
    val loginUserId = "loginUserId"
    val BASE_URL = " http://ostaad.stage02.obdemo.com/api/v1/"
    val DEVICE_TYPE = "device_type"
    val DEVICE_ID = "device_id"
    val API_KEY = "OS-API-KEY"
    val API_SECRET = "OS-API-SIGN"
    val API_TIMESTAMP = "OS-API-TIMESTAMP"

    val API_KEY_VALUE = "f96355923cf08aac54656e10977dd2b2"
    var API_TIMESTAMP_VALUE = ""
    var API_SECRET_VALUE = ""
    val API_SECRET_KEY_VALUE = "943d51473ebdec9b171bc95c07e268556850ee12"

    val NAME = "name"
    val EMAIl = "email"
    val PASSWORD = "password"
    val PHONE_NUMBER = "phone_number"
    val CONFIRM_PASSWORD = "confirm_password"
    val USER_ROLE_ID = " user_role_id"
    val USER_ID = "user_id"
    val USER_ID_DRIVERS = "3"
    val USER_ID_CUSTOMERS = "2"
    val verification_code = "verification_code"
    var ANDROID = "Android"
}