package com.example.androidkotlinbasics.CommanUtils

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.preference.PreferenceManager
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.RatingBar
import android.widget.Toast
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

var ANDROID_DEVICE_ID = "test"
val ROOT_URL = "http://ostaad.stage02.obdemo.com/api/v1/"
val URL_LOGIN: String = ROOT_URL + "login"
val ROOT_URL2 = "http://ostaad.stage02.obdemo.com/"
val verify_verification_code: String = ROOT_URL + "verify-verification-code"

val android = "android"
val URL_SIGNUP: String = ROOT_URL + "signup"
val URL_VERIFY: String = ROOT_URL + "verify-verification-code"
val POST = "POST"
val Authorization = "Authorization"
val loginUserToken = "loginUserToken"

val ENCODE_BASE_URL = "api/v1/"
val singup = ENCODE_BASE_URL + "signup"
val login = ENCODE_BASE_URL + "login"
val loginUserId = "loginUserId"
val BASE_URL = " http://ostaad.stage02.obdemo.com/api/v1/"
val DEVICE_TYPE = "device_type"
val DEVICE_ID = "device_id"
val API_KEY = "OS-API-KEY"
val API_SECRET = "OS-API-SIGN"
val API_TIMESTAMP = "OS-API-TIMESTAMP"

val API_KEY_VALUE = "f96355923cf08aac54656e10977dd2b2"
var API_TIMESTAMP_VALUE = ""
var API_SECRET_VALUE = ""
val API_SECRET_KEY_VALUE = "943d51473ebdec9b171bc95c07e268556850ee12"

val NAME = "name"
val EMAIl = "email"
val PASSWORD = "password"
val PHONE_NUMBER = "phone_number"
val CONFIRM_PASSWORD = "confirm_password"
val USER_ROLE_ID = " user_role_id"
val USER_ID = "user_id"
val USER_ID_DRIVERS = "3"
val USER_ID_CUSTOMERS = "2"
val verification_code = "verification_code"
var ANDROID = "Android"


fun View.visible() {
    visibility = View.VISIBLE
}

fun View.visibilityGone() {
    visibility = View.GONE
}

fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

fun ImageView.displayImage(context: Context, url: String) {
    setImageDrawable(null)
}

fun getStringForGetMethod(Mapvalue: Map<String?, String?>): String? {
    try {
        val finalValue = StringBuilder()
        for ((key, value) in Mapvalue) {
            finalValue.append(key).append("=").append(value).append("&")
        }
        return finalValue.substring(0, finalValue.length - 1)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return ""
}

fun getDeviceId(context: Context?): String? {
    return "123"
}

fun setPref(context: Context?, pref: String?, `val`: String?) {
    val e = PreferenceManager.getDefaultSharedPreferences(context).edit()
    e.putString(pref, `val`)
    e.commit()
}

fun getCurrentTimeStemp(): String? {
    try {
        val tsLong = System.currentTimeMillis() / 1000
        return tsLong.toString()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return ""
}


fun getPref(c: Context?, pref: String?, `val`: String?): String? {
    return PreferenceManager.getDefaultSharedPreferences(c).getString(pref,
            `val`)
}

fun nullSafe(strValue: String?, defaultValue: String): String {
    return if (strValue == null || strValue.trim { it <= ' ' }.length <= 0 || strValue.equals("null", ignoreCase = true)) defaultValue else strValue
}

fun setListViewHeightBasedOnChildren(listView: ListView) {
    val listAdapter = listView.adapter ?: return
    val desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.width, View.MeasureSpec.UNSPECIFIED)
    var totalHeight = 0
    var view: View? = null
    for (i in 0 until listAdapter.count) {
        view = listAdapter.getView(i, view, listView)
        if (i == 0) view.layoutParams = ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
        view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
        totalHeight += view.measuredHeight
    }
    val params = listView.layoutParams
    params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
    listView.layoutParams = params
}

fun setStarColor(ratingBar: RatingBar) {
    val stars = ratingBar.progressDrawable as LayerDrawable
    stars.getDrawable(2).setColorFilter(Color.parseColor("#FDDD4A"), PorterDuff.Mode.SRC_ATOP)
    stars.getDrawable(0).setColorFilter(Color.parseColor("#c7c7c7"), PorterDuff.Mode.SRC_ATOP)
}

fun encodeSha56(data: String): String {
    try {
        val sha256_HMAC = Mac.getInstance("HmacSHA256")
        val secret_key = SecretKeySpec(API_SECRET_KEY_VALUE.toByteArray(Charsets.UTF_8), "HmacSHA256")
        sha256_HMAC.init(secret_key)
        val endCode: String = Base64.encodeToString(sha256_HMAC.doFinal(data.toByteArray(Charsets.UTF_8)), Base64.DEFAULT)
        return endCode.replace("\n", "").replace("\r", "")
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
    return ""
}

fun getUserToken(context: Context): String {
    return "Bearer " + nullSafe(getPref(context, loginUserToken, ""), "")
}
