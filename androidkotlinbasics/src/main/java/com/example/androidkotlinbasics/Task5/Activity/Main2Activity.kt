package com.example.androidkotlinbasics.Task5.Activity

import android.R
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


class Main2Activity : AppCompatActivity() {
    private var sBackground: Drawable? = null
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val label = ImageView(this)
        Toast.makeText(applicationContext,"You are changing orientation...",Toast.LENGTH_SHORT).show();
        if (sBackground == null) {
            sBackground = applicationContext.getDrawable(R.drawable.stat_notify_missed_call)
        }

        label.setBackgroundDrawable(sBackground)
        setContentView(label)
    }
}
