package com.example.androidkotlinbasics.Task5.Utils

/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.SystemClock
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task5.Activity.MainActivity

// Notification ID.
private val NOTIFICATION_ID = 0
private val REQUEST_CODE = 0
private val FLAGS = 0

// TODO: Step 1.1 extension function to send messages (GIVEN)
/**
 * Builds and delivers the notification.
 *
 * @param context, activity context.
 */

fun NotificationManager.sendNotification(messageBody: String, applicationContext: Context) {
    // Create the content intent for the notification, which launches
    // this activity
    // TODO: Step 1.11 create intent
    Log.e("Passed", "Notification 1")

    val contentIntent = Intent(applicationContext, MainActivity::class.java)
    // TODO: Step 1.12 create PendingIntent
    val contentPendingIntent = PendingIntent.getActivity(applicationContext, NOTIFICATION_ID, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    // TODO: Step 2.0 add style

    val eggImage = BitmapFactory.decodeResource(
            applicationContext.resources,
            R.drawable.cooked_egg
    )
    val bigPicStyle = NotificationCompat.BigPictureStyle()
            .bigPicture(eggImage)
            .bigLargeIcon(null)

    // TODO: Step 2.2 add snooze action
    val snoozeIntent = Intent(applicationContext, MainActivity::class.java)
    val snoozePendingIntent: PendingIntent = PendingIntent.getBroadcast(
            applicationContext,
            REQUEST_CODE,
            snoozeIntent,
            FLAGS)

    // TODO: Step 1.2 get an instance of NotificationCompat.Builder
    // Build the notification
    val builder = NotificationCompat.Builder(applicationContext, applicationContext.getString(R.string.egg_notification_channel_id))
            // TODO: Step 1.8 use the new 'breakfast' notification channel
            // TODO: Step 1.3 set title, text and icon to builder
            .setSmallIcon(R.drawable.cooked_egg)
            .setContentTitle(applicationContext.getString(R.string.notification_title))
            .setContentText(messageBody)
            // TODO: Step 1.13 set content intent
            .setContentIntent(contentPendingIntent)
            .setAutoCancel(true)
            // TODO: Step 2.1 add style to builder
            .setStyle(bigPicStyle)
            .setLargeIcon(eggImage)
            // TODO: Step 2.3 add snooze action
            .addAction(R.drawable.egg_icon, applicationContext.getString(R.string.snooze), snoozePendingIntent)
            // TODO: Step 2.5 set priority
            .setPriority(NotificationCompat.PRIORITY_HIGH)

//    applicationContext.getSystemService(Context.NOTIFICATION_SERVICE)
    // TODO: Step 1.4 call notify
//    val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

    /*notificationManager.*/notify(0, builder.build())
}

// TODO: Step 1.14 Cancel all notifications
/**
 * Cancels all notifications.
 *
 */
fun NotificationManager.cancelNotifications() {
    Log.e("Passed", "Notification 3")
    cancelAll()
}


fun CustomNotification(context: Context, packageName: String, layout: Int, title: String, text: String, ticker: String) {

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    val channelId = "i.apps.notifications"
    val description = "Test notification"

    val intent = Intent(context, MainActivity::class.java)/*.apply{
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                Intent.FLAG_ACTIVITY_CLEAR_TASK
    }*/

    val pendingIntent = PendingIntent.getActivity(context,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    val contentView = RemoteViews(packageName,
            layout)
    notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.GREEN
        notificationChannel.enableVibration(false)
        notificationManager.createNotificationChannel(notificationChannel)

        val progressMax = 100
        builder = Notification.Builder(context, channelId)
                .setSmallIcon(R.drawable.cooked_egg)
                .setContentTitle("Notification")
                .setContentText("Downloading")
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setProgress(progressMax, 0, true)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.cooked_egg))
                .setContentIntent(pendingIntent)
//        notificationManager.notify(1234, builder.build())
        notificationManager.notify(1, builder.build())

        Thread(Runnable {
            SystemClock.sleep(2000)
            var progress = 0
            while (progress <= progressMax) {
                SystemClock.sleep(
                        1000
                )
                builder.setProgress(progressMax, progress, true)
                progress += 20
            }

            builder.setContentText("Download complete")
                    .setProgress(0, 0, false)
                    .setOngoing(false)
            notificationManager.notify(1, builder.build())
        }).start()

        Log.e("above 10 ", "above 10")
    } else {

        Log.e("below 10 ", "below 10")
//        var remoteViews = RemoteViews(packageName, layout)
        val contentIntent = Intent(context, MainActivity::class.java)
        val contentPendingIntent: PendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, contentIntent, 0)

//        contentView.setImageViewResource(R.id.imgView, R.drawable.egg_icon_plain)
        contentView.setTextViewText(R.id.txtTitle, title);

        var builder1 = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setTicker(ticker)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setPriority(Notification.PRIORITY_MIN)
                .setContentIntent(contentPendingIntent)

//        builder1.setContent(contentView)

        var notificationmanager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        notificationmanager?.notify(0, builder1.build())
    }

}