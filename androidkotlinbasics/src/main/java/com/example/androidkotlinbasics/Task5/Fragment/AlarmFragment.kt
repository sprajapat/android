package com.example.androidkotlinbasics.Task5.Fragment

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.androidkotlinbasics.R
import kotlinx.android.synthetic.main.fragment_alarm.view.*

class AlarmFragment(context: Context?) : Fragment() {
    var eventListener: OnItemListner? = null

    companion object {
        fun newInstance(context: Context) = AlarmFragment(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_alarm, container, false)
        init(view)
        return view
    }

    private fun init(view: View) {
        view.btnNotification.setOnClickListener {
            eventListener?.shortClick()
        }
    }


    public fun setOnClickListner(eventListener: OnItemListner) {
        this.eventListener = eventListener
    }

    public interface OnItemListner {
        fun shortClick()
    }


}
