package com.example.androidkotlinbasics.Task5.Activity

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.RemoteViews
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.androidkotlinbasics.R
import com.example.androidkotlinbasics.Task5.Fragment.AlarmFragment
import com.example.androidkotlinbasics.Task5.Utils.CustomNotification
import com.example.androidkotlinbasics.Task5.Utils.sendNotification

class MainActivity : AppCompatActivity() {
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "i.apps.notifications"
    private val description = "Test notification"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        var alarmFragment = AlarmFragment(applicationContext)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, alarmFragment)
                .commitNow()

        alarmFragment?.setOnClickListner(object : AlarmFragment.OnItemListner {
            override fun shortClick() {
                CustomNotification(applicationContext, packageName, R.layout.custom_notification, "I Am Notification", "Nothing plzzz", "hello")

            }
        })


    }
}
/*AlarmFragment.newInstance(applicationContext)*/
/*   val intent = Intent(applicationContext, MainActivity::class.java)

                 val pendingIntent = PendingIntent.getActivity(applicationContext,
                         0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                 val contentView = RemoteViews(packageName,
                         R.layout.custom_notification)

                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                     notificationChannel = NotificationChannel( channelId, description, NotificationManager.IMPORTANCE_HIGH)
                     notificationChannel.enableLights(true)
                     notificationChannel.lightColor = Color.GREEN
                     notificationChannel.enableVibration(false)
                     notificationManager.createNotificationChannel(notificationChannel)

                     builder = Notification.Builder(applicationContext, channelId)
                             .setContent(contentView)
                             .setSmallIcon(R.drawable.ic_launcher_background)
                             .setLargeIcon(BitmapFactory.decodeResource(application.resources,
                                     R.drawable.ic_launcher_background))
                             .setContentIntent(pendingIntent)
                 } else {

                     builder = Notification.Builder(applicationContext)
                             .setContent(contentView)
                             .setSmallIcon(R.drawable.ic_launcher_background)
                             .setLargeIcon(BitmapFactory.decodeResource(applicationContext.resources,
                                     R.drawable.ic_launcher_background))
                             .setContentIntent(pendingIntent)
                 }
                 notificationManager.notify(1234, builder.build())*/